﻿using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS481_Final0.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;



namespace CS481_Final0
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        /*private void NewsListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Article itemTapped = (Article)listView.SelectedItem;
            var uri = new Uri(itemTapped.Url);
            Device.OpenUri(uri);
        }*/
        async private void NewsListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Article itemTapped = (Article)listView.SelectedItem;
            await Navigation.PushAsync(new expandNewsPage(itemTapped));
        }


        async private void ContentPage_Appearing(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                Spinner.IsVisible = true;
                Spinner.IsRunning = true;
                var client = new HttpClient();
                var newsApiAddress = "https://newsapi.org/v2/top-headlines?" +
                                     "sources=business-insider&" +
                                     "pageSize=15&" +
                                     "apiKey=027a5e774cb84f6fabbbc81d2650d548";
                var uri = new Uri(newsApiAddress);
                NewsModel newsData = new NewsModel();
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jasonContent = await response.Content.ReadAsStringAsync();
                    newsData = JsonConvert.DeserializeObject<NewsModel>(jasonContent);
                }
                Spinner.IsRunning = false;
                Spinner.IsVisible = false;
                NewsListView.ItemsSource = newsData.Articles;
            }
            else
            {
                await DisplayAlert("Alert", "No Internet Connection", "OK");
            }
        }
    }
}