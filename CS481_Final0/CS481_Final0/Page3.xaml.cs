﻿using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS481_Final0.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFGloss;
using Microcharts;
using Entry = Microcharts.Entry;

namespace CS481_Final0
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }



        async void Handle_New_Stock(object sender, System.EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                string ticker = tickerinput.Text.ToLower();

                //Handle API calls to the quote url used for daily values
                var client1 = new HttpClient();
                var stock_address1 = "https://api.iextrading.com/1.0/stock/" + ticker + "/quote";
                var uri1 = new Uri(stock_address1);

                StockQuoteModel StockImmediate = new StockQuoteModel();
                var response1 = await client1.GetAsync(uri1);
                if (response1.IsSuccessStatusCode)
                {
                    var jsonContent1 = await response1.Content.ReadAsStringAsync();
                    StockImmediate = JsonConvert.DeserializeObject<StockQuoteModel>(jsonContent1);
                }
                //Update the XAML with this information
                
                    //All these statements throw errors exceot for the symbol one inexplicably
                //symbol.Text = StockImmediate.symbol; //This one works but none others do for some reason
                //latestPrice.Text = StockImmediate.latestPrice;
                //daychange.Text = StockImmediate.change.ToString();
                //company.Text = StockImmediate.companyName;
                //sector_ = StockImmediate.sector;
                //ytdchange = StockImmediate.ytdChange;
                else
                {
                    await DisplayAlert("Alert", "No Internet Connection", "OK");
                }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////  First three and last three fields populated, now fill out Microcharts with 1month data  ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //Handle API calls to the month's data url used for the chart's values
                var client2 = new HttpClient();
                var stock_address2 = "https://api.iextrading.com/1.0/stock/" + ticker + "/chart/1m";
                var uri2 = new Uri(stock_address2);

                //StockHistoryModel StockMonth = new StockHistoryModel();

                var StockMonth = new StockHistoryList(); //This will hold the list of HistoryModels from the API
                var response2 = await client2.GetAsync(uri2);
                if (response2.IsSuccessStatusCode)
                {
                    var jsonContent2 = await response2.Content.ReadAsStringAsync();
                    StockMonth = JsonConvert.DeserializeObject<StockHistoryList>(jsonContent2);
                }






                //Put a month's worth of closing values into an array of doubles (~25 or so elements)
               
                var entrylist = new List<Entry>()
                {
                    //This should be a loop if at all possible, going from 0 to list_of_definitions.Count
                    //new Entry((float)StockMonth.closingvalues.ElementAt(0).close),//1 month ago
                    for (int i = 0; i < StockMonth.closingvalues.Count(); i++)
                    {
                        new Entry((float)StockMonth.closingvalues.ElementAt(i).close);
                    }
                }
                    
           
                  
                var history = new LineChart() { Entries = entrylist };

                TimeChart.Chart = history;

                else
                {
                    await DisplayAlert("Alert", "No Internet Connection", "OK");
                }
            }
        }
    }
}


/*
            <Label x:Name="{Binding symbol}"/>
            <Label x:Name="{Binding latestPrice}"/>
            <Label x:Name="{Binding change}"/>

            <microcharts:ChartView x:Name="TimeChart" />

            <Label x:Name="{Binding companyName}"/>
            <Label x:Name="{Binding sector}"/>
            <Label x:Name="{Binding ytdChange}"/>  
*/
/*using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS481_Final0.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
//using XFGloss;

namespace CS481_Final0
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {

        public Page3()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async private void Handle_New_Stock(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                var client = new HttpClient();
                var StockQuoteURL = "https://api.iextrading.com/1.0/stock/market/list/gainers";
                var StockQuote__uri = new Uri(StockQuoteURL);

                var StockQuoteInfo = new ObservableCollection<TopStock>();
                var response = await client.GetAsync(StockQuote__uri);

                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    StockQuoteInfo = JsonConvert.DeserializeObject<ObservableCollection<TopStock>>(jsonContent);
                }
                string[] companyNames = new string[50];
                var topPercentChange = 0;
                var topPerformer = " ";
                for (int i = 0; i < StockQuoteInfo.Count; i++)
                {
                    if (topPercentChange < StockQuoteInfo[i].changePercent)
                    {
                        topPerformer = StockQuoteInfo[i].companyName;
                    }
                }
                TopStockList.ItemsSource = StockQuoteInfo;

                marqueeLabel.Text = "Top Performing Stock: " + topPerformer;
            }
            else
            {
                await DisplayAlert("Alert", "No Internet Connection", "OK");
            }


        }
    }
}
*/

/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Final0
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
		public Page3 ()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
*/
