﻿using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Timers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS481_Final0.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFGloss;


namespace CS481_Final0
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page0 : ContentPage
    {
        
        public Page0()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        
        async private void ContentPage_Appearing0(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            { 
                var client = new HttpClient();
                var topStocksAddress = "https://api.iextrading.com/1.0/stock/market/list/gainers";
                var uri = new Uri(topStocksAddress);

                var frontPageStocks = new ObservableCollection<TopStock>();
                var response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();
                    frontPageStocks = JsonConvert.DeserializeObject<ObservableCollection<TopStock>>(jsonContent);
                }
                string[] companyNames = new string[50];
                var topPercentChange = 0;
                var topPerformer = " ";
                for (int i = 0; i < frontPageStocks.Count; i++)
                {
                    if (topPercentChange < frontPageStocks[i].changePercent)
                    {
                        topPerformer = frontPageStocks[i].companyName;
                    }
                }
                TopStockList.ItemsSource = frontPageStocks;

                //var newStock = "random";

                marqueeLabel.Text = "Top Performing Stock: " + topPerformer;
            }
            else
            {
                await DisplayAlert("Alert", "No Internet Connection", "OK");
            }


        }

        async private void TopStockList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            TopStock itemTapped = (TopStock)listView.SelectedItem;
            await Navigation.PushAsync(new expandStockPage(itemTapped));
        }
    }
}

// https://api.iextrading.com/1.0/stock/market/list/gainers
// 